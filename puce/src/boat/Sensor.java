package boat;

import java.lang.StringBuilder;

public abstract class Sensor implements IControlSurface {
	public abstract double getMinValue();
	public abstract double getMaxValue();
	
	private Engine engine;
	private boolean isOk;
	private double currentValue;

	public Sensor(Engine engine) {
		this.engine = engine;
	}

	public Engine getEngine() {
		return this.engine;
	}

	public void performStartupChecks() {
		this.setIsOk(this.engine != null);
	}

	public boolean getIsOk() {
		return this.isOk;
	}
	
	public void setIsOk(boolean isOk) {
		this.isOk = isOk;
	}

	protected void setCurrentValue(double currentValue) {
		this.currentValue = currentValue;
	}

	public double getCurrentValue() {
		return this.currentValue;
	}

	public abstract String getNiceName();
	public abstract String getUnit();

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append(this.getNiceName());
		b.append(" : ");
		b.append(this.getCurrentValue());
		b.append(this.getUnit());

		return b.toString();
	}

}
