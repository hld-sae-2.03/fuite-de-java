package boat;

import java.util.ArrayList;
import java.util.List;

import sensors.FuelSensor;
import sensors.OilTempSensor;
import sensors.EngineTempSensor;
import sensors.WaterTempSensor;

public class Boat implements IStartable, IControlSurface {
	public static final String BRAND = "MARQUE";
	public static final String MODEL = "Modèle";
	public static final String NAME = "Nom";

	private List<IControlSurface> controls;

	private Engine engine;
	private Throttle throttle;
	private Rudder rudder;

	private boolean isOk;


	public Boat() {
		Engine engine = new Engine();
		Throttle throttle = new Throttle(engine);
		Rudder rudder = new Rudder();

		this.engine = engine;
		this.throttle = throttle;
		this.rudder = rudder;

		this.controls = new ArrayList<IControlSurface>();
		this.controls.add(new FuelSensor(engine));
		this.controls.add(new OilTempSensor(engine));
		this.controls.add(new EngineTempSensor(engine));
		this.controls.add(new WaterTempSensor(engine));
		this.controls.add(throttle);
		this.controls.add(rudder);
	}


	public Throttle getThrottle() {
		return this.throttle;
	}

	public Engine getEngine() {
		return this.engine;
	}

	public Rudder getRudder() {
		return this.rudder;
	}

	public boolean getIsOk() {
		this.statusUpdate();

		return this.isOk;
	}

	public boolean getStarted() {
		return this.engine.getStarted();
	}


	public void setIsOk(boolean isOk) {
		this.isOk = isOk;
	}

	
	public void performStartupChecks() {
		for (IControlSurface ctrl : this.controls) {
			ctrl.performStartupChecks();
		}

		this.setIsOk(true);
		this.statusUpdate();
	}

	public boolean start() {
		this.performStartupChecks();

		return this.getIsOk() && this.engine.start();
	}

	public boolean stop() {
		return this.engine.stop();
	}


	private void statusUpdate() {
		for (IControlSurface ctrl : this.controls) {
			if (!ctrl.getIsOk()) {
				this.setIsOk(false);

				break;
			}
		}
	}
}