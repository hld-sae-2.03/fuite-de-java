package boat;

public class Throttle implements IControlSurface {
	private Engine engine;
	private int value;
	private boolean isOk;

	private static final int MIN_VALUE = 0;
	private static final int MAX_VALUE = 0;

	private Throttle() {
		this.value = 0;
	}

	public Throttle(Engine engine) {
		this();

		this.engine = engine;
	}

	public void setValue(int value) {
		if (!this.getIsOk() // on ne peut pas changer les gaz tant que le moteur est éteint
			|| value < Throttle.MIN_VALUE
			|| value > Throttle.MAX_VALUE) {
			return;
		}

		int targetRPM = engine.getMaxRPM() * (value / 100);

		this.engine.setRPM(targetRPM);

		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

	public void performStartupChecks() {
		this.isOk = this.engine != null;
	}

	public boolean getIsOk() {
		return this.isOk && this.engine.getStarted();
	}

	public void setIsOk(boolean isOk) {
		this.isOk = isOk;
	}
}
