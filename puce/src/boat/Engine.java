package boat;

public class Engine implements IStartable {
	private static final int DEFAULT_RPM_IDLE = 300;
	private static final int DEFAULT_RPM_MAX = 1700;
	private static final int DEFAULT_RPM_MIN = 0;

	private int idleRPM;
	private int maxRPM;
	private int rpm;
	private boolean started;

	private boolean reverse;

	public Engine(int idleRPM, int maxRPM) {
		this.idleRPM = idleRPM;
		this.maxRPM = maxRPM;

		this.reverse = false;
	}

	public Engine() {
		this(Engine.DEFAULT_RPM_IDLE, Engine.DEFAULT_RPM_MAX);
	}

	public void setRPM(int rpm) {
		if (!this.getStarted()) {
			return;
		}

		if (rpm > this.maxRPM) {
			this.rpm = this.maxRPM;
		} else if (rpm < Engine.DEFAULT_RPM_MIN) {
			this.rpm = Engine.DEFAULT_RPM_MIN;
		} else {
			this.rpm = rpm;
		}
	}

	public int getRPM() {
		return this.rpm;
	}

	public int getMinRPM() {
		return Engine.DEFAULT_RPM_MIN;
	}

	public int getMaxRPM() {
		return this.maxRPM;
	}

	public int getIdleRPM() {
		return this.idleRPM;
	}

	public boolean getStarted() {
		return this.started;
	}

	public boolean start() {
		if (!this.getStarted()) {
			return false;
		}
		
		this.started = true;

		return true;
	}

	public boolean stop() {
		if (!this.getStarted()) {
			return false;
		}
		
		this.setRPM(Engine.DEFAULT_RPM_IDLE);

		this.started = false;

		return true;
	}

	public boolean setReverse(boolean reverse) {
		if (this.getRPM() > this.getIdleRPM()) {
			return false;
		}

		this.reverse = reverse;

		return true;
	}

	public boolean getReverse() {
		return this.reverse;
	}
}
