package boat;

public class Rudder implements IControlSurface {
	private static final double MIN_TURNFACTOR = -1;
	private static final double MAX_TURNFACTOR = 1;
	private static final double REST_TURNFACTOR = 0;

	private boolean isOk;
	private double turnFactor;

	public Rudder() {
		this.turnFactor = Rudder.REST_TURNFACTOR;
		this.isOk = false;
	}

	public void performStartupChecks() {
		this.isOk = true;
	}

	public boolean getIsOk() {
		return this.isOk;
	}

	public void setTurnFactor(double turnFactor) {
		if (this.getIsOk() 
			&& turnFactor >= Rudder.MIN_TURNFACTOR 
			&& turnFactor <= Rudder.MAX_TURNFACTOR) 
		{
			this.turnFactor = turnFactor;
		}

	}

	public double getTurnFactor() {
		return this.turnFactor;
	}
	
	public void setIsOk(boolean isOk) {
		this.isOk = isOk;
	}
}
