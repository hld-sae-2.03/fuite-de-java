package boat;

public interface IStartable {
	public boolean start();
	public boolean stop();
	public boolean getStarted();
}
