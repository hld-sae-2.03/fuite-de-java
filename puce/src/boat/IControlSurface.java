package boat;

public interface IControlSurface {
	public void performStartupChecks();
	public boolean getIsOk();
	public void setIsOk(boolean isOk);
}
