package sensors;

import boat.Engine;
import boat.Sensor;

public class FuelSensor extends Sensor {
	private static final int MIN_VALUE = 0;
	private static final int MAX_VALUE = 100;
	private static final String NICE_NAME = "Niveau de carburant";
	private static final String NICE_UNIT = "L";

	public FuelSensor(Engine engine) {
		super(engine);
	}

	public double getMinValue() {
		return MIN_VALUE;
	}

	public double getMaxValue() {
		return MAX_VALUE;
	}

	public String getNiceName() {
		return NICE_NAME;
	}
	
	public String getUnit() {
		return NICE_UNIT;
	}
}
