package sensors;

import boat.Engine;
import boat.Sensor;

public class EngineTempSensor extends Sensor {
	private static final int MIN_VALUE = 0;
	private static final int MAX_VALUE = 100;
	private static final String NICE_NAME = "Température moteur";
	private static final String NICE_UNIT = "°C";

	public EngineTempSensor(Engine engine) {
		super(engine);
	}

	public double getMinValue() {
		return MIN_VALUE;
	}

	public double getMaxValue() {
		return MAX_VALUE;
	}

	public String getNiceName() {
		return NICE_NAME;
	}
	
	public String getUnit() {
		return NICE_UNIT;
	}
}
